cloudkitty-dashboard (19.0.0-2) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090404).

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Dec 2024 17:18:21 +0100

cloudkitty-dashboard (19.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Oct 2024 16:54:48 +0200

cloudkitty-dashboard (19.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Sep 2024 22:54:52 +0200

cloudkitty-dashboard (19.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 17 Sep 2024 09:56:48 +0200

cloudkitty-dashboard (18.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 15:37:20 +0200

cloudkitty-dashboard (18.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 Mar 2024 11:20:04 +0100

cloudkitty-dashboard (17.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 05 Oct 2023 11:43:45 +0200

cloudkitty-dashboard (17.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Fri, 15 Sep 2023 16:32:51 +0200

cloudkitty-dashboard (16.0.0-3) unstable; urgency=medium

  * Cleans properly (Closes: #1043921).

 -- Thomas Goirand <zigo@debian.org>  Mon, 14 Aug 2023 13:04:19 +0200

cloudkitty-dashboard (16.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 19 Jun 2023 16:15:06 +0200

cloudkitty-dashboard (16.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 22 Mar 2023 15:08:10 +0100

cloudkitty-dashboard (16.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 06 Mar 2023 09:19:17 +0100

cloudkitty-dashboard (15.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 05 Oct 2022 22:20:20 +0200

cloudkitty-dashboard (15.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Sep 2022 04:13:11 +0200

cloudkitty-dashboard (15.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 13 Sep 2022 14:57:00 +0200

cloudkitty-dashboard (14.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Mar 2022 22:40:32 +0200

cloudkitty-dashboard (14.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 27 Mar 2022 13:29:45 +0200

cloudkitty-dashboard (14.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Mar 2022 13:39:54 +0100

cloudkitty-dashboard (13.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Oct 2021 17:28:18 +0200

cloudkitty-dashboard (13.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Sep 2021 13:46:15 +0200

cloudkitty-dashboard (13.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.
  * (Build-)depends on minimum horizon >= 20.0.0+git2020.09.21.27036cc0eb.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Sep 2021 17:33:51 +0200

cloudkitty-dashboard (12.0.0-4) unstable; urgency=medium

  * Add remove-price-tab-when-launching-instance.patch.
  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 14:59:15 +0200

cloudkitty-dashboard (12.0.0-3) experimental; urgency=medium

  * Add debian/python3-cloudkitty-dashboard.maintscript to remove old conffiles
    in the /etc/openstack-dashboard/enable folder.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 May 2021 11:21:21 +0200

cloudkitty-dashboard (12.0.0-2) experimental; urgency=medium

  * Package the enable folder in
    /usr/lib/python3/dist-packages/openstack_dashboard/local/enabled.
  * Add Breaks: python3-django-horizon (<< 3:19.2.0-2~).

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 May 2021 11:25:20 +0200

cloudkitty-dashboard (12.0.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Apr 2021 22:44:00 +0200

cloudkitty-dashboard (12.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 25 Mar 2021 11:34:00 +0100

cloudkitty-dashboard (11.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Uploading to unstable.
  * Fixed debian/watch.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Sat, 17 Oct 2020 22:30:34 +0200

cloudkitty-dashboard (11.0.0~rc1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 23 Sep 2020 15:46:47 +0200

cloudkitty-dashboard (10.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 13 May 2020 15:08:42 +0200

cloudkitty-dashboard (10.0.0~rc1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 10 May 2020 12:08:33 +0200

cloudkitty-dashboard (10.0.0~rc1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Fix wrong Vcs-*.

  [ Thomas Goirand ]
  * Move the package to the horizon-plugins subgroup.
  * New upstream release.
  * Fixed horizon (build-)dependency.

 -- Thomas Goirand <zigo@debian.org>  Sat, 25 Apr 2020 00:11:27 +0200

cloudkitty-dashboard (9.0.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 16 Oct 2019 21:08:54 +0200

cloudkitty-dashboard (8.1.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.

  [ Michal Arbet ]
  * Do not move files from source, copy it,
    kolla deployment expects that files are in /usr/lib...

 -- Michal Arbet <michal.arbet@ultimum.io>  Thu, 22 Aug 2019 11:54:44 +0200

cloudkitty-dashboard (8.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Updated (build-)depends for this release, removing versions satisfied in
    Buster.
  * Fixed d/copyright syntax.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2019 16:57:03 +0200

cloudkitty-dashboard (8.0.0-3) unstable; urgency=medium

  * Redesign horizon plugin
  * d/control: Add me to uploaders field
  * d/copyright: Add me to copyright file

 -- Michal Arbet <michal.arbet@ultimum.io>  Mon, 21 Jan 2019 19:51:40 +0100

cloudkitty-dashboard (8.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Dec 2018 05:05:10 +0100

cloudkitty-dashboard (8.0.0-1) experimental; urgency=medium

  * Initial release. (Closes: #888436)

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Aug 2018 09:36:54 +0200
